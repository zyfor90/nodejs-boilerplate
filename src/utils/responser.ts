import { Response } from "express";

export interface Responser {
    meta: Meta;
    data: any;
}

interface Meta {
    code: number;
    message: string;
    status: string;
}

export class APIResponser {
    public successResponse = (
        response: Response,
        message: string,
        data: any = null,
        httpStatusCode: number
    ): any => {
        const res: Responser = {
            meta: {
                code: httpStatusCode,
                message: message,
                status: "success",
            },
            data: data,
        };

        return response.status(httpStatusCode).json(res);
    };

    public errorResponse = (
        response: Response,
        message: string,
        httpStatusCode: number,
        data: any = null
    ): any => {
        const res: Responser = {
            meta: {
                code: httpStatusCode,
                message: message,
                status: "error",
            },
            data: [],
        };

        return response.status(httpStatusCode).json(res);
    };

    public internalErrorResponse = (
        response: Response,
        error: Error,
        httpStatusCode: number,
        data: any = null
    ): any => {
        const res: Responser = {
            meta: {
                code: httpStatusCode,
                message: error.message,
                status: "error",
            },
            data: [],
        };

        return response.status(httpStatusCode).json(res);
    };
}
