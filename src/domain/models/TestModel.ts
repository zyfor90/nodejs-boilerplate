import { Document, Model, Schema, Connection } from "mongoose";

interface Test extends Document {
    name: string;
    email: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;
}

const testSchema = new Schema<Test>({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

export class TestModel {
    public Model = async (connection: Connection): Promise<Model<Test>> => {
        return connection.model<Test>("Test", testSchema);
    };
}
