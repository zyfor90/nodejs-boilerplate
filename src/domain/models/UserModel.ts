import { Document, Model, Schema, Connection } from "mongoose";

export interface User extends Document {
    name: string;
    email: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;
}

const userSchema = new Schema<User>({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

export class UserModel {
    public Model = async (connection: Connection): Promise<Model<User>> => {
        return connection.model<User>("User", userSchema);
    };
}
