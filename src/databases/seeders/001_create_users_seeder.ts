import { UserModel, User } from "domain/models/UserModel";
import { Connection, Model } from "mongoose";

export class Seeder {
    private file: string;
    private connection: Connection;
    private user: UserModel;

    constructor(connection: Connection, file: string) {
        this.connection = connection;
        this.file = file;
        this.user = new UserModel();
    }

    async up(): Promise<void> {
        try {
            const checkCollection = await this.connection.db
                .listCollections({ name: "users" })
                .hasNext();

            if (!checkCollection) {
                throw new Error(
                    `collection tidak di temukan di file : ${this.file}`
                );
            }

            const TestEntities = await this.user.Model(this.connection);

            // Hapus semua data dari koleksi (opsional)
            await TestEntities.deleteMany({});

            // Masukkan data ke dalam koleksi
            console.log(`Running seeder: ${this.file} In Progress`);
            await TestEntities.insertMany([
                // Tambahkan data yang ingin Anda masukkan ke dalam koleksi di sini
                {
                    name: "Febri",
                    email: "febriansyah602@gmail.com",
                    password: "testing123",
                    created_at: Date.now,
                    updated_at: Date.now,
                },
                {
                    name: "User 1",
                    email: "febriansyah1719@gmail.com",
                    password: "testing321",
                    created_at: Date.now,
                    updated_at: Date.now,
                },
            ]);
            console.log(`Running seeder: ${this.file} Successfully`);
        } catch (error: any) {
            console.log("Seeder error:", error.message);
        }
    }
}
