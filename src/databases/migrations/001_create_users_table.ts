import { UserModel } from "domain/models/UserModel";
import { Connection } from "mongoose";

export class Migration extends UserModel {
    private file: string;
    private connection: Connection;

    constructor(connection: Connection, file: string) {
        super();
        this.connection = connection;
        this.file = file;
    }

    async up(): Promise<void> {
        try {
            const checkCollection = await this.connection.db
                .listCollections({ name: "users" })
                .hasNext();

            if (checkCollection) {
                return;
            }

            const UserEntities = await this.Model(this.connection);

            // Create the collection if it does not exist
            console.log(`Running migration: ${this.file} In Progress`);
            await UserEntities.createCollection();
            console.log(`Running migration: ${this.file} Successfully`);
        } catch (error: any) {
            console.log("Migration error:", error.message);
        }
    }
}
