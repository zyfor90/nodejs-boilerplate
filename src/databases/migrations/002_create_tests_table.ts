import { TestModel } from "domain/models/TestModel";
import { Connection } from "mongoose";

export class Migration extends TestModel {
    private file: string;
    private connection: Connection;

    constructor(connection: Connection, file: string) {
        super();
        this.connection = connection;
        this.file = file;
    }

    async up(): Promise<void> {
        try {
            const checkCollection = await this.connection.db
                .listCollections({ name: "tests" })
                .hasNext();

            if (checkCollection) {
                return;
            }

            const TestEntities = await this.Model(this.connection);

            // Create the collection if it does not exist
            console.log(`Running migration: ${this.file} In Progress`);
            await TestEntities.createCollection();
            console.log(`Running migration: ${this.file} Successfully`);
        } catch (error: any) {
            console.log("Migration error:", error.message);
        }
    }
}
