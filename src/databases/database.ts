import { abort } from "process";
import { constants, db_config } from "setting";
import fs from "fs";
import path from "path";
import { Connection } from "mongoose";

// * Import Mongoose
const mongoose = require("mongoose");

export class Database {
    public async SetConnection(): Promise<Connection> {
        try {
            // * Get Database Environment
            const { DB_HOST, DB_PORT, DB_NAME } = db_config;

            // * Check the environment values
            if (
                DB_HOST === constants.ErrUndefined ||
                DB_PORT === constants.ErrUndefined ||
                DB_NAME === constants.ErrUndefined
            ) {
                throw new Error(
                    `DB_HOST: ${DB_HOST} - DB_PORT: ${DB_PORT} - DB_NAME: ${DB_NAME}`
                );
            }
            // * Try to connect with MongoDB
            const conn: Connection = await mongoose.createConnection(
                `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
                {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                }
            );

            // Check if the connection is successful
            const connectionPromise = new Promise<Connection>(
                (resolve, reject) => {
                    // Check for connection errors
                    conn.on("error", (error: any) => {
                        if (error.message.includes("ECONNREFUSED")) {
                            reject("Database server is not running");
                        } else if (error.message.includes("ENOTFOUND")) {
                            reject("Database not found");
                        } else {
                            reject("internal server error");
                        }
                    });

                    conn.once("open", async () => {
                        const collections = await conn.db
                            .listCollections()
                            .toArray();
                        if (collections.length === 0) {
                            reject(`Database "${DB_NAME}" disconnected`);
                        } else {
                            console.log(`Database Connection opened.`);
                            resolve(conn);
                        }
                    });
                }
            );

            await connectionPromise;

            return conn;
        } catch (error) {
            console.error("Error connecting to the database:", error);
            throw error; // Rethrow the error so that the caller can handle it if needed
        }
    }

    // Method to close the connection
    public async CloseConnection(connection: Connection): Promise<void> {
        if (connection) {
            try {
                console.log("Database connection closed.");
                await connection.close();
            } catch (error) {
                console.error("Error closing the database connection:", error);
            }
        }
    }

    public Ping = async () => {
        try {
            // Await the Promise to resolve
            const setConnection = await this.SetConnection();

            // Close the connection when it's no longer needed
            await this.CloseConnection(setConnection);
        } catch (error) {
            console.error("Error:", error);
            abort();
        }
    };

    public Migrate = async () => {
        let connection: Connection | null = null;

        try {
            const migrationFiles = fs
                .readdirSync(path.join(__dirname, "migrations"))
                .sort();

            connection = await this.SetConnection();
            for (const file of migrationFiles) {
                if (file.endsWith(".ts")) {
                    const migration = await import(
                        path.join(__dirname, "migrations", file)
                    );

                    const getMigration = new migration.Migration(
                        connection,
                        file
                    );

                    if (migration && typeof getMigration.up === "function") {
                        await getMigration.up(connection, file);
                    }
                }
            }

            await this.CloseConnection(connection);
        } catch (error) {
            if (connection != null) {
                await this.CloseConnection(connection);
            }
            console.error("Migration error:", error);
        }
    };

    public Seed = async () => {
        let connection: Connection | null = null;

        try {
            const migrationFiles = fs
                .readdirSync(path.join(__dirname, "seeders"))
                .sort();

            connection = await this.SetConnection();
            for (const file of migrationFiles) {
                if (file.endsWith(".ts")) {
                    const seeder = await import(
                        path.join(__dirname, "seeders", file)
                    );

                    const getSeeder = new seeder.Seeder(connection, file);

                    if (seeder && typeof getSeeder.up === "function") {
                        await getSeeder.up(connection, file);
                    }
                }
            }

            await this.CloseConnection(connection);
        } catch (error) {
            if (connection != null) {
                await this.CloseConnection(connection);
            }
            console.error("Migration error:", error);
        }
    };
}
