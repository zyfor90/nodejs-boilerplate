import { IUserController, InitUserController } from "api/user/UserController";
import { Router } from "express";

const router = Router();

const UserController: IUserController = InitUserController();

// * Set Routes Here.
router.get("/user", UserController.GetUser);

export default router;
