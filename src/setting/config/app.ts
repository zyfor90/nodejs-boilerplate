// * Define type data here
interface ApplicationConfig {
    readonly APP_NAME: string | undefined;
    readonly APP_PORT: string | undefined;
    readonly APP_ENV: string | undefined;
}

// Define value and object here
export const app_config: ApplicationConfig = {
    APP_NAME: process.env.APP_NAME,
    APP_PORT: process.env.APP_PORT || "4000",
    APP_ENV: process.env.APP_ENV,
};
