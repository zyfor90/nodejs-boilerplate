// * Define type data here
interface MyError {
    readonly ErrUndefined: undefined;
}

// Define value and object here
export const constants: MyError = {
    ErrUndefined: undefined,
};
