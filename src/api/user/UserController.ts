import { BaseController } from "api/base";
import { IUserService, InitUserService } from "api/user/UserService";
import { Request, Response } from "express";
import { Responser } from "utils/responser";

export interface IUserController {
    GetUser: (req: Request, res: Response) => Promise<Responser>;
}

export const InitUserController = (): IUserController => {
    // Create an instance of UserController with the userService property
    const userController = new UserController();

    // Return the UserController instance which implements IUserController
    return userController;
};

class UserController extends BaseController implements IUserController {
    private userService: IUserService;

    constructor() {
        // Consturctor BaseController
        super();

        // Define Service Here.
        this.userService = InitUserService();
    }

    public GetUser = async (
        req: Request,
        res: Response
    ): Promise<Responser> => {
        try {
            const userService = await this.userService.GetUserByID();
            return this.successResponse(
                res,
                "berhasil mendapatkan data pengguna",
                userService,
                200
            );
        } catch (error) {
            return this.internalErrorResponse(res, error as Error, 500);
        }
    };
}
