import { Connection } from "mongoose";
import { BaseService } from "api/base";
import { IUserRepository, InitUserRepository } from "api/user/UserRepository";
import { User } from "domain/models/UserModel";

export interface IUserService {
    GetUserByID: () => Promise<User>;
}

export const InitUserService = (): IUserService => {
    // Create an instance of UserService with the userService property
    const userService = new UserService();

    // Return the UserService instance which implements IUserService
    return userService;
};

class UserService extends BaseService implements IUserService {
    private userRepository: IUserRepository;

    constructor() {
        // Consturctor BaseService
        super();

        // Define Repository Here.
        this.userRepository = InitUserRepository();
    }

    GetUserByID = async (): Promise<User> => {
        // * Set Connection Here. if error the try catch from controller will handle it.
        const conn: Connection = await this.SetConnection();

        try {
            const user: User = await this.userRepository.GetUserByID(conn);

            return user;
        } catch (error) {
            await this.CloseConnection(conn);
            throw new Error((error as Error).message);
        } finally {
            await this.CloseConnection(conn);
        }
    };
}
