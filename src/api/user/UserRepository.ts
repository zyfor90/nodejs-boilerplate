import { Connection } from "mongoose";
import { UserModel, User } from "domain/models/UserModel";

export interface IUserRepository {
    GetUserByID: (conn: Connection) => Promise<User>;
}

export const InitUserRepository = (): IUserRepository => {
    // Create an instance of UserRepository with the userRepository property
    const userRepository = new UserRepository();

    // Return the UserRepository instance which implements IUserRepository
    return userRepository;
};

class UserRepository implements IUserRepository {
    private user: UserModel;

    constructor() {
        this.user = new UserModel();
    }

    GetUserByID = async (conn: Connection): Promise<User> => {
        const UserEntities = await this.user.Model(conn);

        const getUser: User | null = await UserEntities.findOne({
            name: "Febri",
        }).exec();

        if (getUser === null) {
            throw new Error("Pengguna tidak ditemukan");
        }

        return getUser;
    };
}
