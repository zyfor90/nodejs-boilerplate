import { APIResponser } from "utils/responser";
import { Database } from "databases/database";

export class BaseController extends APIResponser {}
export class BaseService extends Database {}
