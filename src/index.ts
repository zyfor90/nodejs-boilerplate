import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import routes from "router/api";
import { Database } from "databases/database";
import { app_config } from "setting";

export const app = express();

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan("combined"));

const db = new Database();
db.Ping(); // Check Connection to Database

// * Define Routes here.
app.use("/", routes);

app.listen(app_config.APP_PORT, () => {
    console.log(`=======================================`);
    console.log(`Project Name     : ${app_config.APP_NAME}`);
    console.log(`Running on Port  : ${app_config.APP_PORT}`);
    console.log(`Enviroment       : ${app_config.APP_ENV}`);
    console.log(`=======================================`);
});
